const input=document.querySelector("textarea")
const btn=document.querySelector(".save")

var count=0;
var count2=0;
btn.addEventListener('click',addInList);

function addInList(e){
    const completed=document.querySelector(".done");
    const notCompleted=document.querySelector(".pending");
    const list=document.createElement('li');
    const dt=document.createElement('date');
    const doneBtn=document.createElement('button');
    const deleteBtn=document.createElement('button');
    const docount=document.querySelector('.docounter');
    const tskcount=document.getElementById('counter');
    const newDiv=document.createElement('div');
    const newDiv2=document.createElement('div');
    doneBtn.innerHTML='<button type="submit" class="btn btn-primary" id="donebtn">Mark As Read</button>';
    deleteBtn.innerHTML='<button type="submit" class="btn" id="deletebtn">&#x2715</button>';
    var date=new Date();
    dt.className='date';
    dt.innerHTML=date.toUTCString();
    if(input.value!==''){
        list.textContent=input.value;
        input.value='';
        notCompleted.appendChild(list);
        list.classList.add("fl");
        list.appendChild(newDiv);
        newDiv.appendChild(dt);
        count=count+1;
        tskcount.innerHTML=count;
        newDiv.appendChild(deleteBtn);
        newDiv.appendChild(doneBtn);
    }
    
    doneBtn.addEventListener('click', function(){
        const parent=this.parentNode;
        const superParent=parent.parentNode;
        superParent.classList.add("taskdone");
        parent.remove();
        count=count-1;
        count2=count2+1;
        tskcount.innerHTML=count;
        docount.innerHTML=count2;
        completed.appendChild(superParent);
        newDiv2.appendChild(deleteBtn);
        newDiv2.appendChild(dt);
        superParent.appendChild(newDiv2);
        doneBtn.style.display='none';
    });
    deleteBtn.addEventListener('click',function(){
        const parent=this.parentNode;
        const grandParent=parent.parentNode;

        if(grandParent.matches('.taskdone')){
            count2=count2-1;
            docount.innerHTML=count2;
        }
        else{
            count=count-1;
            tskcount.innerHTML=count;
        }
        grandParent.remove();
    });
}